package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level4Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level4Controller() throws SQLException {
    }

    @GetMapping("/level4") public String level4Form(Model model) throws SQLException {

        Level4 level4 = new Level4();
        String query = "SELECT id, name, description FROM article";

        ResultSet rs = stmt.executeQuery(query);
        level4.setArticles(new ArrayList<>());
        while (rs.next()) {
            level4.getArticles().add(new Article(rs.getString(2), rs.getString(3)));
        }
        model.addAttribute("level4", level4);
        return "level4";
    }

    @PostMapping("/level4") public String level4Submit(@ModelAttribute Level4 level4, Model model) throws SQLException {

        String query = "SELECT id, name, description FROM article";
        if(level4.getSearch() != null && !level4.getSearch().equals("")) {
            query += " WHERE description LIKE '%" + level4.getSearch() + "%'";
        }

        level4.setQuery(query);
        ResultSet rs = stmt.executeQuery(level4.getQuery());
        level4.setArticles(new ArrayList<>());
        while (rs.next()) {
            level4.getArticles().add(new Article(rs.getString(2), rs.getString(3)));
        }
        model.addAttribute("level4", level4);
        return "level4";
    }

}
