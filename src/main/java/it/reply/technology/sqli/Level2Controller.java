package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level2Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level2Controller() throws SQLException {
    }

    @GetMapping("/level2") public String level2Form(Model model) {
        model.addAttribute("level2", new Level2());
        return "level2";
    }

    @PostMapping("/level2") public String level2Submit(@ModelAttribute Level2 level2, Model model) throws SQLException {

        level2.setQuery("SELECT * FROM login WHERE username = '" + level2.getUsername() + "' AND password = '" + level2.getPassword() + "' AND admin = false");
        ResultSet rs = stmt.executeQuery(level2.getQuery());
        if (rs.next()) {
            boolean admin = rs.getBoolean(4);
            if(admin) {
                level2.setResult("Admin login success");
            }
            else {
                level2.setResult("Admin login failure");
            }
        }
        else {
            level2.setResult("Failure");
        }
        model.addAttribute("level2", level2);
        return "level2";
    }

}
