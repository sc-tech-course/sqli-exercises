package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level8Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level8Controller() throws SQLException {
    }

    private ArrayList<Article> getArticles() throws SQLException {
        String query = "SELECT id, name, description FROM article";
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<Article> articles = new ArrayList<>();
        while (rs.next()) {
            articles.add(new Article(rs.getString(2), rs.getString(3)));
        }
        return articles;
    }

    @GetMapping("/level8") public String level8Form(Model model) throws SQLException {

        Level8 level8 = new Level8();
        model.addAttribute("level8", level8);
        return "level8";
    }

    @PostMapping("/level8") public String level8Submit(@ModelAttribute Level8 level8, Model model) throws SQLException {

        String query = "INSERT INTO message(title, content) VALUES ('" + level8.getTitle() + "','" + level8.getMessage() + "')";

        level8.setQuery(query);
        stmt.execute(query);
        level8.setResult("Your message has been received");
        model.addAttribute("level8", level8);
        return "level8";
    }

}
