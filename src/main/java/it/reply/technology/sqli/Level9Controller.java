package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;
import java.util.regex.Pattern;

@Controller public class Level9Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level9Controller() throws SQLException {
    }

    private ArrayList<Article> getArticles() throws SQLException {
        String query = "SELECT id, name, description FROM article";
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<Article> articles = new ArrayList<>();
        while (rs.next()) {
            articles.add(new Article(rs.getString(2), rs.getString(3)));
        }
        return articles;
    }

    @GetMapping("/level9") public String level9Form(Model model) throws SQLException {

        Level9 level9 = new Level9();
        level9.setArticles(getArticles());
        model.addAttribute("level9", level9);
        return "level9";
    }

    @PostMapping("/level9") public String level9Submit(@ModelAttribute Level9 level9, Model model) throws SQLException {

        String query = "INSERT INTO article(name, description) VALUES ('" + level9.getItem() + "','" + level9.getDescription() + "')";
        level9.setQuery(query);

        if(!Pattern.matches("^[A-Za-z0-9=() ,']+$", level9.getItem()) || !Pattern.matches("^[A-Za-z0-9=() ,']+$", level9.getDescription())) {
            level9.setResult("SQL injection detected!");
        }
        else {
            level9.setResult("");
            stmt.execute(query);
        }
        level9.setArticles(getArticles());
        model.addAttribute("level9", level9);
        return "level9";
    }

}
