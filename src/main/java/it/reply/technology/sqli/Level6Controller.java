package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.regex.Pattern;

@Controller public class Level6Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level6Controller() throws SQLException {
    }

    @GetMapping("/level6") public String level6Form(Model model) {
        model.addAttribute("level6", new Level6());
        return "level6";
    }

    @PostMapping("/level6") public String level6Submit(@ModelAttribute Level6 level6, Model model) throws SQLException {

        boolean login = false;
        boolean sqli = false;

        level6.setQuery("SELECT * FROM login WHERE username = '" + level6.getUsername() + "' AND password = '" + level6.getPassword() + "'");
        ResultSet rs = stmt.executeQuery(level6.getQuery());
        if (rs.next()) {
            login = true;
        }
        if(login && !Pattern.matches("^[A-Za-z0-9_]+$", level6.getPassword())) {
            sqli = true;
        }

        if(sqli) {
            level6.setResult("Login would have been successful, but I caught your SQL injection!");
        } else if(login) {
            level6.setResult("Login success");
        } else {
            level6.setResult("Login failure");
        }

        model.addAttribute("level6", level6);
        return "level6";
    }

}

/*
7 information schema
8 totally blind (error)
9 hti+
10 file write + read
11 totally blind (time)
 */
