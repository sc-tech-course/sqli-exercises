package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level10Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level10Controller() throws SQLException {
    }

    private ArrayList<Article> getArticles() {
        String query = "SELECT id, name, description FROM article";
        ArrayList<Article> articles = new ArrayList<>();

        try {
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                articles.add(new Article(rs.getString(2), rs.getString(3)));
            }
        } catch (Exception e) {
            // Will not leak any errors
        }
        return articles;
    }

    @GetMapping("/level10") public String level8Form(Model model) {

        Level10 level10 = new Level10();
        model.addAttribute("level10", level10);
        return "level10";
    }

    @PostMapping("/level10") public String level10Submit(@ModelAttribute Level10 level10, Model model) {
        long start = System.nanoTime();

        String query = "INSERT INTO message(title, content) VALUES ('" + level10.getTitle() + "','" + level10.getMessage() + "')";

        level10.setQuery(query);
        try {
            stmt.execute(query);
        } catch (Exception e) {
            // Will not leak any errors
        }

        long finish = System.nanoTime();
        level10.setResult("Your message has been sent in " + (finish - start) + " nanoseconds");
        model.addAttribute("level10", level10);
        return "level10";
    }

}
