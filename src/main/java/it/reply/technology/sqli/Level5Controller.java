package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level5Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level5Controller() throws SQLException {
    }

    private ArrayList<Article> getArticles() throws SQLException {
        String query = "SELECT id, name, description FROM article";
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<Article> articles = new ArrayList<>();
        while (rs.next()) {
            articles.add(new Article(rs.getString(2), rs.getString(3)));
        }
        return articles;
    }

    @GetMapping("/level5") public String level5Form(Model model) throws SQLException {

        Level5 level5 = new Level5();
        level5.setArticles(getArticles());
        model.addAttribute("level5", level5);
        return "level5";
    }

    @PostMapping("/level5") public String level5Submit(@ModelAttribute Level5 level5, Model model) throws SQLException {

        String query = "INSERT INTO article(name, description) VALUES ('" + level5.getItem() + "','" + level5.getDescription() + "')";

        level5.setQuery(query);
        stmt.execute(query);
        level5.setArticles(getArticles());
        model.addAttribute("level5", level5);
        return "level5";
    }

}
