package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level3Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level3Controller() throws SQLException {
    }

    @GetMapping("/level3") public String level3Form(Model model) throws SQLException {
        Level3 level3 = new Level3();
        String query = "SELECT id, name, description FROM article";

        ResultSet rs = stmt.executeQuery(query);
        level3.setArticles(new ArrayList<>());
        while (rs.next()) {
            level3.getArticles().add(new Article(rs.getString(2), rs.getString(3)));
        }
        model.addAttribute("level3", level3);
        return "level3";
    }

    @PostMapping("/level3") public String level3Submit(@ModelAttribute Level3 level3, Model model) throws SQLException {

        level3.setQuery("SELECT * FROM article WHERE description LIKE '%" + level3.getSearch() + "%'");
        ResultSet rs = stmt.executeQuery(level3.getQuery());
        level3.setArticles(new ArrayList<>());
        while (rs.next()) {
            level3.getArticles().add(new Article(rs.getString(2), rs.getString(3)));
        }
        model.addAttribute("level3", level3);
        return "level3";
    }

}
