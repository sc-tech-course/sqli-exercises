package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level1Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level1Controller() throws SQLException {
    }

    @GetMapping("/level1") public String level1Form(Model model) {
        model.addAttribute("level1", new Level1());
        return "level1";
    }

    @PostMapping("/level1") public String level1Submit(@ModelAttribute Level1 level1, Model model) throws SQLException {

        level1.setQuery("SELECT * FROM login WHERE username = '" + level1.getUsername() + "' AND password = '" + level1.getPassword() + "'");
        ResultSet rs = stmt.executeQuery(level1.getQuery());
        if (rs.next()) {
            level1.setSuccess(true);
        }
        model.addAttribute("level1", level1);
        return "level1";
    }

}
