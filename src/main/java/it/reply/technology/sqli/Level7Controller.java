package it.reply.technology.sqli;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller public class Level7Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    public Level7Controller() throws SQLException {
    }

    @GetMapping("/level7") public String level7Form(Model model) throws SQLException {
        Level7 level7 = new Level7();
        String query = "SELECT id, name, description FROM article";

        ResultSet rs = stmt.executeQuery(query);
        level7.setArticles(new ArrayList<>());
        while (rs.next()) {
            level7.getArticles().add(new Article(rs.getString(2), rs.getString(3)));
        }
        model.addAttribute("level7", level7);
        return "level7";
    }

    @PostMapping("/level7") public String level7Submit(@ModelAttribute Level7 level7, Model model) throws SQLException {

        level7.setQuery("SELECT * FROM article WHERE description LIKE '%" + level7.getSearch() + "%'");
        ResultSet rs = stmt.executeQuery(level7.getQuery());
        level7.setArticles(new ArrayList<>());
        while (rs.next()) {
            level7.getArticles().add(new Article(rs.getString(2), rs.getString(3)));
        }
        model.addAttribute("level7", level7);
        return "level7";
    }

}
